package com.sortingApp;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;


public class NumberSorterTest {

    @ParameterizedTest
    @ValueSource(strings = {"", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"})
    public void testSingleElement(String value) {
        int[] input = {Integer.parseInt(value)};
        int[] expected = Arrays.copyOf(input, 1);
        Arrays.sort(expected);

        NumberSorter sorter = new NumberSorter();
        int[] result = sorter.sortNumbers(input);

        assertArrayEquals(expected, result);
    }

    @ParameterizedTest
    @ValueSource(strings = {"0 1 2 3 4 5 6 7 8 9", "0 1 2 3 4 5 6 7 8 9 10 11 12"})
    public void testMultipleElements(String inputString) {
        String[] inputs = inputString.split("\\s+");
        int[] input = Arrays.stream(inputs).mapToInt(Integer::parseInt).toArray();
        int[] expected = Arrays.copyOf(input, input.length);
        Arrays.sort(expected);

        NumberSorter sorter = new NumberSorter();
        int[] result = sorter.sortNumbers(input);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testEmptyArray() {
        int[] input = {};
        int[] expected = {};

        NumberSorter sorter = new NumberSorter();
        int[] result = sorter.sortNumbers(input);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testArrayWithTenElements() {
        int[] input = {9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
        int[] expected = Arrays.copyOf(input, input.length);
        Arrays.sort(expected);

        NumberSorter sorter = new NumberSorter();
        int[] result = sorter.sortNumbers(input);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testArrayWithMoreThanTenElements() {
        int[] input = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, -1};
        int[] expected = Arrays.copyOf(input, input.length);
        Arrays.sort(expected);

        NumberSorter sorter = new NumberSorter();
        int[] result = sorter.sortNumbers(input);

        assertArrayEquals(expected, result);
    }

}
