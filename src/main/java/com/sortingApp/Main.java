package com.sortingApp;

import java.util.Arrays;



public class Main {
    public static void main(String[] args) {
        NumberSorter sorter = new NumberSorter();
        int[] numbers = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            if (!args[i].isEmpty()) {
                numbers[i] = Integer.parseInt(args[i]);
            }
        }
        int[] sorted = sorter.sortNumbers(numbers);
        System.out.println("Sorted numbers: " + Arrays.toString(sorted));
    }
}
