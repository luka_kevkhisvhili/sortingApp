package com.sortingApp;

import java.util.Arrays;

public class NumberSorter {

    public int[] sortNumbers(int[] array) {
        if (array == null) {
            throw new IllegalArgumentException("Array is null");
        }

        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                int min = array[j];
                int max = array[j + 1];
                if (min > max) {
                    int current = min;
                    array[j] = max;
                    array[j + 1] = current;
                }
            }
        }
        return array;
    }
}